package com.client.graphql;


import client.LaunchDetailsQuery;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.client.graphql.body.GraphQLQuery;
import com.client.graphql.utils.ApolloClientUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



class TestRunner {

    @Test
    void graphqlEndpoint(){
        Assertions.assertNotNull(System.getenv("URL"));
    }

    @Test
    void testRunner(){
        GraphqlClient graphqlClient = GraphqlClient.getGraphqlClient();
        Assertions.assertNotNull(graphqlClient, "Graphql is Null");
        ApolloCall<LaunchDetailsQuery.Data> launchDetailsQuery = GraphQLQuery.getLaunchDetailsQuery("83");
        Assertions.assertNotNull(launchDetailsQuery, "Graphql Payload is Null");
        LaunchDetailsQuery.Data data = graphqlClient.hitGraphqlApi(launchDetailsQuery);
        Assertions.assertNotNull(data, "Response is Null");
    }

    @Test
    void checkGraphqlSingleton(){
        GraphqlClient graphqlClient1 = GraphqlClient.getGraphqlClient();
        GraphqlClient graphqlClient2 = GraphqlClient.getGraphqlClient();
        GraphqlClient graphqlClient3 = GraphqlClient.getGraphqlClient();
        Assertions.assertNotNull(graphqlClient1);
        Assertions.assertEquals(graphqlClient1.hashCode(),graphqlClient2.hashCode(),graphqlClient3.hashCode());
        System.out.println(graphqlClient3);
        System.out.println(graphqlClient1);
        System.out.println(graphqlClient2);
    }

    @Test
    void checkApolloClientSingleton() {
        ApolloClient apolloClient1 = ApolloClientUtils.buildApolloClient();
        ApolloClient apolloClient2 = ApolloClientUtils.buildApolloClient();
        ApolloClient apolloClient3 = ApolloClientUtils.buildApolloClient();
        Assertions.assertNotNull(apolloClient1);
        Assertions.assertEquals(apolloClient1.hashCode(),apolloClient2.hashCode(),apolloClient3.hashCode());
        System.out.println(apolloClient3);
        System.out.println(apolloClient2);
        System.out.println(apolloClient1);
    }

}
