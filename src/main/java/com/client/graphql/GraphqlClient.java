package com.client.graphql;

import com.client.graphql.utils.ApolloClientUtils;
import com.apollographql.apollo.ApolloCall;
import org.jetbrains.annotations.NotNull;
import java.util.logging.Logger;


public class GraphqlClient{

    private static final Logger LOG = Logger.getLogger(GraphqlClient.class.getName());
    private static GraphqlClient graphqlClient = null;

    private GraphqlClient(){}

    public static GraphqlClient getGraphqlClient(){
        if(graphqlClient == null) {
            graphqlClient = new GraphqlClient();
        }
        return graphqlClient;
    }

    public <T> T hitGraphqlApi(@NotNull ApolloCall<T> apolloCall){
        try {
            return ApolloClientUtils.callGraphqlApi(apolloCall).join().getData();
        }catch (Exception e){
            LOG.info("Error Occurred " + e.getMessage());
        }
        return null;
    }

}
