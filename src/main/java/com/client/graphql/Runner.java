package com.client.graphql;

import client.LaunchDetailsQuery;
import com.client.graphql.body.GraphQLQuery;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.Logger;

public class Runner {

    private static final Logger LOG = Logger.getLogger(Runner.class.getSimpleName());

    public static void main(String[] args) {
        GraphqlClient graphqlClient = GraphqlClient.getGraphqlClient();
        Instant startTime = Instant.now();
        LOG.info("Waiting");

        LaunchDetailsQuery.Data response = graphqlClient.hitGraphqlApi(GraphQLQuery.getLaunchDetailsQuery("83"));

        Instant endTime = Instant.now() ;
        String t = String.format("Total Time %s", Duration.between(startTime, endTime).toString());
        LOG.info(t);
        t = response.toString();
        LOG.info(t);
    }
}
