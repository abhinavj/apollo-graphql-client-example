package com.client.graphql.body;

import client.LetsCancelTripMutation;
import com.client.graphql.utils.ApolloClientUtils;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;

public class GraphQLMutation {

    private static final ApolloClient apolloClient = ApolloClientUtils.buildApolloClient();

    private GraphQLMutation(){}

    public static ApolloCall<LetsCancelTripMutation.Data> getLetsCancelTripMutation(String lid){
        return apolloClient.mutate(LetsCancelTripMutation
                .builder()
                .id(lid)
                .build());
    }
}
