package com.client.graphql.body;


import client.LaunchDetailsQuery;
import com.client.graphql.utils.ApolloClientUtils;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;

public class GraphQLQuery {

    private static final ApolloClient apolloClient = ApolloClientUtils.buildApolloClient();

    public static ApolloCall<LaunchDetailsQuery.Data> getLaunchDetailsQuery(String lid){
        return apolloClient.query(LaunchDetailsQuery
                .builder()
                .id(lid)
                .build());
    }

}
