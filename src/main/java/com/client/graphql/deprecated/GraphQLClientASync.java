package com.client.graphql.deprecated;


import client.LaunchDetailsQuery;
import client.LetsCancelTripMutation;
import com.client.graphql.STATUS;
import com.client.graphql.utils.ApolloClientUtils;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.Logger;

@Deprecated
public class GraphQLClientASync implements IGraphqlClient {

    private static final Logger LOG = Logger.getLogger(GraphQLClientASync.class.getName());
    private static final ApolloClient apolloClient = ApolloClientUtils.buildApolloClient();

    @Override
    public String hitQueryApi(String lid) {
        final String[] id = {""};
        id[0] = "hello";
        apolloClient.query(LaunchDetailsQuery.builder().id("83").build())
                .enqueue(new ApolloCall.Callback<LaunchDetailsQuery.Data>() {

                    @Override
                    public void onResponse(@NotNull Response<LaunchDetailsQuery.Data> response) {
                        LOG.info("Apollo Launch site: " + Objects.requireNonNull(response.getData()).launch());
                        id[0] += STATUS.SUCCESS.toString();
                    }

                    @Override
                    public void onFailure(@NotNull ApolloException e) {
                        LOG.info("Apollo Error" + e);
                        id[0] += STATUS.FAILED.toString();
                    }
                });

        while (!id[0].equals("success") && !id[0].equals("failed"));

        return id[0];
    }

    @Override
    public String hitMutationApi(String lid) {
        final String[] id = {""};
        apolloClient.mutate(LetsCancelTripMutation.builder().id("83").build())
                .enqueue(new ApolloCall.Callback<LetsCancelTripMutation.Data>() {

                    @Override
                    public void onResponse(@NotNull Response<LetsCancelTripMutation.Data> response) {
                        LOG.info("Apollo Launch site: " + Objects.requireNonNull(response.getData()));
                        id[0] = STATUS.SUCCESS.toString();
                    }

                    @Override
                    public void onFailure(@NotNull ApolloException e) {
                        LOG.info("Apollo Error" + e);
                        id[0] = STATUS.FAILED.toString();
                    }
                });

        while (!id[0].equals("success") && !id[0].equals("failed"));

        return id[0];
    }
}
