package com.client.graphql.deprecated;

@Deprecated
public interface IGraphqlClient {

    String hitQueryApi(String lid);

    String hitMutationApi(String lid);
}
