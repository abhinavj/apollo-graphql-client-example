package com.client.graphql.deprecated;

import client.LaunchDetailsQuery;
import client.LetsCancelTripMutation;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.client.graphql.STATUS;
import com.client.graphql.body.GraphQLMutation;
import com.client.graphql.body.GraphQLQuery;
import com.client.graphql.utils.ApolloClientUtils;

import java.util.Objects;
import java.util.logging.Logger;

/**
 * @deprecated (when, why, etc...)
 */
@Deprecated
public class GraphQLClientSync implements IGraphqlClient{

    private static final Logger LOG = Logger.getLogger(GraphQLClientSync.class.getName());

    @Override
    public String hitQueryApi(String lid){
        try {
            ApolloCall<LaunchDetailsQuery.Data> queryCall = GraphQLQuery.getLaunchDetailsQuery(lid);

            Response<LaunchDetailsQuery.Data> completableFuture = ApolloClientUtils.callGraphqlApi(queryCall).join();

            LaunchDetailsQuery.Data data = completableFuture.getData();

            LOG.info(data.toString());

            if (Objects.requireNonNull(data.launch()).id().equals(lid))
                return STATUS.SUCCESS.toString();

        }catch (Exception e){
            LOG.info("Error Occurred " + e.getMessage());
        }
        LOG.info("Graphql Api Call Failed");
        return STATUS.FAILED.toString();
    }

    @Override
    public String hitMutationApi(String lid){
        try {
            ApolloCall<LetsCancelTripMutation.Data> mutationCall = GraphQLMutation.getLetsCancelTripMutation(lid);
            LetsCancelTripMutation.Data data = ApolloClientUtils.callGraphqlApi(mutationCall).join().getData();
            LOG.info(data.toString());
            if (Objects.requireNonNull(data.cancelTrip()).success())
                return STATUS.SUCCESS.toString();
        }catch (Exception e){
            LOG.info("Error Occurred " + e.getMessage());
        }
        LOG.info("Graphql Api Call Failed");
        return STATUS.FAILED.toString();
    }
}
