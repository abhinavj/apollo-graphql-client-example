package com.client.graphql.utils;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

public class ApolloClientUtils {

    private static final Logger LOG = Logger.getLogger(ApolloClientUtils.class.getName());
    private static final String SERVER_URL = Objects.requireNonNull(System.getenv("URL"));
    private static ApolloClient apolloClient;

    private ApolloClientUtils(){}

    public static <T> CompletableFuture<Response<T>> callGraphqlApi(@NotNull ApolloCall<T> apolloCall) {
        CompletableFuture<Response<T>> completableFuture = new CompletableFuture<>();

        completableFuture.whenComplete((tResponse, throwable) -> {
            if (completableFuture.isCancelled()) {
                completableFuture.cancel(true);
            }
        });

        apolloCall.enqueue(new ApolloCall.Callback<T>() {

            @Override
            public void onResponse(@NotNull Response<T> response) {
                completableFuture.complete(response);
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                completableFuture.completeExceptionally(e);
            }
        });
        return completableFuture;
    }

    public static ApolloClient buildApolloClient() {
        if(apolloClient == null) {
            LOG.info("Graphql Endpoint: " + SERVER_URL);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .authenticator(Authenticator.NONE)
                    .addInterceptor(interceptor)
                    .build();

            apolloClient = ApolloClient.builder()
                    .serverUrl(SERVER_URL)
                    .okHttpClient(okHttpClient)
                    .build();
        }
        return apolloClient;
    }
}
