package com.client.graphql;

public enum STATUS {
    SUCCESS("SUCCESS"),
    FAILED("FAILED");

    final String value;

    STATUS(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
