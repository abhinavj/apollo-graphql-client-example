# How to Use Graphql Client

##### 1. First Create your graphql payload file and Schema file and add in `src/main/java/graphql/` folder
   
##### 2. You have to build ApolloClient and generate diffrent graphql operation as
1. For mutation use `apolloClient.mutation(<Graphql Payload>)`
2. For Query use `apolloClient.query(<Graphql Payload>)`
##### 3. Apollo generate java code from Schema and payload file, and Class generated have same name as payload file
* eg. payload file in `src/main/java/graphql/<dir if any>/LaunchDetailsQuery.graphql` and  respective class is 
  generated in <br> `<dir if any>LaunchDetailsQuery`
##### 4. you can create Graphql Payload using builder or by constructor 
1. by builder `LaunchDetailsQuery.builder().id(lid).build()`
2. by constructor `new LaunchDetailsQuery(lid)`
##### 5. Code Example    
```text
    private static final ApolloClient apolloClient = ApolloClientUtils.buildApolloClient();

    public static ApolloCall<LaunchDetailsQuery.Data> getLaunchDetailsQuery(String lid){
        return apolloClient.query(LaunchDetailsQuery
                .builder()
                .id(lid)
                .build());
    }
    
    Create your method which will return graphql payload
```
>You can create different class for Query, Mutation and Subscription or Single class As per your choice

3. Now execute Graphql Api

```text
    // build your graphql client
    GraphqlClient graphqlClient = GraphqlClient.getGraphqlClient();
    
    // generate your graphql payload
    ApolloCall<LaunchDetailsQuery.Data> launchDetailsQuery = GraphQLQuery.getLaunchDetailsQuery("83");    
    
    // call graphql Api and get response , return null when error or no response received
    LaunchDetailsQuery.Data response = graphqlClient.hitGraphqlApi(launchDetailsQuery);

```